import React, { Component } from 'react';
import './App.css';
import firebase from 'firebase'
import { StyledFirebaseAuth } from 'react-firebaseui'

firebase.initializeApp({
  apiKey: "",
  authDomain: ""

})

class App extends Component {
  state = {
    isSignedIn: false
  }


  uiConfig = {
    signInFlow: "popup",
    signInOptions: [
      firebase.auth.EmailAuthProvider.PROVIDER_ID,
      firebase.auth.GoogleAuthProvider.PROVIDER_ID,
      firebase.auth.FacebookAuthProvider.PROVIDER_ID,
      firebase.auth.GithubAuthProvider.PROVIDER_ID
    ],
    callbacks: {
      signInSuccess: () => false
    }
  }

  componentDidMount = () => {
    firebase.auth().onAuthStateChanged(user => {
      this.setState({ isSignedIn: !!user });
    })
  }


  render() {
    return (
      <div className="App">

        {this.state.isSignedIn ?
          (
            <div>
              <div>Signed In!</div>
              <button onClick={() => firebase.auth().signOut()}>Sign out</button>
            </div>
          ) : (
            <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />)
        }

      </div>
    );
  }
}

export default App;

